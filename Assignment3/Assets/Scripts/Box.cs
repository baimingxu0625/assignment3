﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] boxstuff;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BreakBox() {
        int choose = 0;
        int i = Random.Range(1, 100);
        if (i >=1 && i < 20) {
            choose = 3;
        }
        else if (i >=20 && i < 40)
        {
            choose = 2;
        }
        else if (i >= 40 && i < 60)
        {
            choose = 1;
        }
        else {
            choose = 0;
        }
        Destroy(gameObject);
        Instantiate(boxstuff[choose],gameObject.transform.position,Quaternion.identity);


    }
}
