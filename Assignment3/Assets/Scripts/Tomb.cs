﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tomb : MonoBehaviour
{
    [SerializeField]
    public GameObject enemyPFs;
    public float spawnTimer;
    public float spawnTime;
    private int curzombienum;
    private int zombienum = 5;
    private bool canspawn = true;
    // Start is called before the first frame update
    void Start()
    {
        
        spawnTime = 3f;
        spawnTimer = Random.Range(2, 8);
    }

    // Update is called once per frame
    void Update()
    {
        if (canspawn == true)
        {
            if (spawnTimer > 0)
                spawnTimer -= Time.deltaTime;
            if (spawnTimer < 0)
                spawnTimer = 0;
            if (spawnTimer == 0)
            {
                spawnTimer = spawnTime;
                Spawn();
            }

        }
    }

    private void Spawn()
    {
        curzombienum++;
        if(curzombienum<zombienum)
        Instantiate(
            enemyPFs,gameObject.transform.position,
            Quaternion.identity
            );

    }
}
