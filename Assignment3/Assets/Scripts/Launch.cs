﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launch : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;
    // Start is called before the first frame update
    float xspeed;
    float yspeed;
    void Start()
    {
        xspeed = Random.Range(-1, 1);
        yspeed = Random.Range(5f, 8f);
        rigidbody2D = GetComponent<Rigidbody2D>();
        Invoke("Setspeed", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        
    }

    private void Setspeed() {

        xspeed = Random.Range(-1, 1);
        yspeed = -9;
    }

    private void Move() {

        rigidbody2D.velocity = new Vector2(xspeed,yspeed);
    }

    public void Dealhit()
    {
        GameController.instance.Updatescore(10);
        rigidbody2D.simulated = false;
        Destroy(gameObject);
    }
}
