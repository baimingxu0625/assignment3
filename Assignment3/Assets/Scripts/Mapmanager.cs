﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapmanager : MonoBehaviour
{
    
    public GameObject[] outWall;
    public GameObject[] floor;
    public GameObject[] wall;
    public GameObject batSpawn;
    public GameObject ballSpawn;
    public GameObject tomb;
    public GameObject box;
    public GameObject lava;
    public GameObject exit;
    public int tombcount;
    
    // Start is called before the first frame update

    public int rows = 12;
    public int cols = 22;

    public int wallcount = 6;
    //public int boxcount = 6;
    public int level ;
    public Transform mapholder;

    private List<Vector2> positionList = new List<Vector2>();

    

    void Start()
    {
        level = GameController.instance.levelnum;
        InitMap();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitMap()
    {
        
        mapholder = new GameObject("Map").transform;
        for (int x = 0; x < cols; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if (x == 0)
                {
                    GameObject go = GameObject.Instantiate(outWall[1], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);

                }
                else if (y == 0)
                {
                    GameObject go = GameObject.Instantiate(outWall[0], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);
                }
                else if (x == cols - 1)
                {
                    GameObject go = GameObject.Instantiate(outWall[3], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);
                }
                else if (y == rows - 1)
                {
                    GameObject go = GameObject.Instantiate(outWall[2], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);
                }


                else
                {
                    int index = Random.Range(0, floor.Length);

                    GameObject go = GameObject.Instantiate(floor[index], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);

                }
            }
        }
        for (int x = 0; x < cols; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if ((x == 0 && y == 0) || (x == 0 && y == rows - 1) || (x == cols - 1 && y == 0) || (x == cols - 1 && y == rows - 1))
                {
                    GameObject go = GameObject.Instantiate(outWall[4], new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                    go.transform.SetParent(mapholder);

                }


            }
        }




        positionList.Clear();
        for (int x = 2; x < cols - 2; x++)
        {
            for (int y = 2; y < rows - 2; y++)
            {
                positionList.Add(new Vector2(x, y));

            }

        }
        for (int i = 0; i < wallcount; i++)
        {

            Vector2 pos = RandomPositon();

            int wallindex = Random.Range(0, wall.Length);
            GameObject go = GameObject.Instantiate(wall[wallindex], pos, Quaternion.identity) as GameObject;
            go.transform.SetParent(mapholder);
        }

        //1+level
        int boxcount = Random.Range(1, 1 + level);

        for (int i = 0; i < boxcount; i++)
        {

            Vector2 pos = RandomPositon();
            
            GameObject go = GameObject.Instantiate(box, pos, Quaternion.identity) as GameObject;
            go.transform.SetParent(mapholder);


        }

        int lavacount = Random.Range(2, 2*level);
        for (int i = 0; i < lavacount; i++)
        {

            Vector2 pos = RandomPositon();

            GameObject go = GameObject.Instantiate(lava, pos, Quaternion.identity) as GameObject;
            go.transform.SetParent(mapholder);
            

        }

        tombcount = (int)(1 + level*0.5);
        
        for (int i = 0; i < tombcount; i++)
        {

            Vector2 pos = RandomPositon();

            GameObject tombgo = GameObject.Instantiate(tomb, pos, Quaternion.identity) as GameObject;
            tombgo.transform.SetParent(mapholder);
            
        }

        GameObject exitgo=Instantiate(exit, new Vector2(cols - 2, rows - 2), Quaternion.identity) as GameObject;
        exitgo.transform.SetParent(mapholder);

        int batcount = level;
        if (level >= 2) {
            for (int i = 0; i < batcount; i++)
            {

                Vector2 pos = RandomPositon();

                GameObject go = GameObject.Instantiate(batSpawn, pos, Quaternion.identity) as GameObject;
                go.transform.SetParent(mapholder);

            }
        }

        int ballcount = level-1;
        if (level >= 3)
        {
            for (int i = 0; i < batcount; i++)
            {

                Vector2 pos = RandomPositon();

                GameObject go = GameObject.Instantiate(ballSpawn, pos, Quaternion.identity) as GameObject;
                go.transform.SetParent(mapholder);

            }
        }








    }

    private Vector2 RandomPositon() {
        int positionIndex = Random.Range(0, positionList.Count);
        Vector2 pos = positionList[positionIndex];
        positionList.RemoveAt(positionIndex);
        return pos;

    }

    

}