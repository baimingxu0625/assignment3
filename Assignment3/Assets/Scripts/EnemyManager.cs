﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField]
    private int curzombienum ;
    private int zombienum = 10;
    public static EnemyManager instance;
    // Start is called before the first frame update
    public float attackTimer;
    public float attackTime;
    private GameObject[] tomb;
    
    public GameObject[] enemyPFs;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        Invoke("SpawnOnstart",5f);
        //tomb = (GameObject[])Mapmanager.instance.tombGo;
        attackTimer = 0;
        attackTime = 10.0f;
    }

    // Update is called once per frame
    void Update()
    {
         if (attackTimer>0)
            attackTimer-= Time.deltaTime;
          if (attackTimer<0)
            attackTimer=0;
          if(attackTimer == 0){
            Swapingame();
            attackTimer =attackTime;}
           

        
       }

    void Swapingame() {

       ;
        if (curzombienum >= zombienum) return;

        int enemyId = Random.Range(0, enemyPFs.Length);
        int posiId = Random.Range(0, tomb.Length);


        Spawn(enemyId, posiId);
    }

    public void SpawnOnstart()
    {
        for (int i=0;i<tomb.Length;++i) {
            int enemyId = Random.Range(0,enemyPFs.Length);
            Spawn(enemyId,i);
        }

    }

    void Spawn(int enemyIdex,int posIndex) {
        curzombienum++;
        Instantiate(
            enemyPFs[enemyIdex],
            tomb[posIndex].transform.position,
            Quaternion.identity
            );

    }
    public void ResetZombie()
    {
        curzombienum = 0;
        SpawnOnstart();
    }

    
}
