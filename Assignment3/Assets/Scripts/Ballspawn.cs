﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ballspawn : MonoBehaviour
{
    [SerializeField]
    public GameObject enemyPFs;
    public float spawnTimer;
    public float spawnTime;
    private int curbatnum;
    private int batnum = 4;
    private bool canspawn = true;
    // Start is called before the first frame update
    void Start()
    {

        spawnTime = 3f;
        spawnTimer = Random.Range(5, 8);
    }

    // Update is called once per frame
    void Update()
    {
        if (canspawn == true)
        {
            if (spawnTimer > 0)
                spawnTimer -= Time.deltaTime;
            if (spawnTimer < 0)
                spawnTimer = 0;
            if (spawnTimer == 0)
            {
                spawnTimer = spawnTime;
                Spawn();
            }

        }
    }

    private void Spawn()
    {
        curbatnum++;
        if (curbatnum < batnum)
            Instantiate(
                enemyPFs, gameObject.transform.position,
                Quaternion.identity
                );

    }
}
