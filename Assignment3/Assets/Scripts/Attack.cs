﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{

    [SerializeField]
    private float keeptime = 2f;
    [SerializeField]
    private float moveSpeed = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
       

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right*moveSpeed*Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "enemy")
        {
            other.GetComponent<Enemy>().Dealhit();
            Destroy(gameObject);

        }
        else if (other.tag == "box") {
            other.GetComponent<Box>().BreakBox();
            Destroy(gameObject);
        }
        else if (other.tag == "bat")
        {
            other.GetComponent<Bat>().Dealhit();
            Destroy(gameObject);
        }
        else if (other.tag == "ball")
        {
            other.GetComponent<Launch>().Dealhit();
            Destroy(gameObject);
        }

        else {
            Destroy(gameObject);

        }

        
    }


}
